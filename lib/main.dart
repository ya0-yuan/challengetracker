import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:i_am_rich/TaskListPage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Firebase.initializeApp(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Scaffold(
            body: Center(child: Text(snapshot.error.toString())),
          );
        }
        return MaterialApp(

          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            fontFamily: 'avenir',
            scaffoldBackgroundColor: Colors.grey[900],
            primarySwatch: Colors.orange,
          ),
          home: TaskListPage(),
        );
      },
    );

  }
}