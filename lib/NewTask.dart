import 'package:flutter/material.dart';
import 'package:i_am_rich/TaskListPage.dart';
import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;
import 'package:i_am_rich/database_services.dart';
import 'package:intl/intl.dart';
import 'dart:async';


class NewTask extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          fontFamily: 'avenir'
      ),
      home: MyNewTask(),
    );
  }
}
class MyNewTask extends StatefulWidget {
  @override
  _MyNewTaskState createState() => _MyNewTaskState();
}

class _MyNewTaskState extends State<MyNewTask> {
  bool isComplet = false; // just for now
  TextEditingController todoTitleController = TextEditingController();
  String filterType = "Today";

  DateTime _startDate = DateTime.now();
  DateTime _endDate = DateTime.now();

  Future displayDateRangePicker(BuildContext context) async {
    final List<DateTime> picked = await DateRagePicker.showDatePicker(
        context: context,
        initialFirstDate: _startDate,
        initialLastDate: _endDate,
        firstDate: new DateTime(DateTime.now().year - 50),
        lastDate: new DateTime(DateTime.now().year + 50));
    if (picked != null && picked.length == 2) {
      setState(() {
        _startDate = picked[0];
        _endDate = picked[1];
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery
        .of(context)
        .size
        .width;
    return SimpleDialog(
      contentPadding: EdgeInsets.symmetric(
        horizontal: 25,
        vertical: 20,
      ),
      backgroundColor: Colors.grey[800],
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      title: Row(
        children: [
          Text(
            "Add A Task",
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
            ),
          ),
          Spacer(),
          IconButton(
            icon: Icon(
              Icons.cancel,
              color: Colors.grey,
              size: 30,
            ),
            onPressed: () => Navigator.pop(context),
          )
        ],
      ),
      children: [
        Divider(),
        TextFormField(
          controller: todoTitleController,
          style: TextStyle(
            fontSize: 18,
            height: 1.5,
            color: Colors.white,
          ),
          autofocus: true,
          decoration: InputDecoration(
            hintText: "eg. exercise",
            hintStyle: TextStyle(color: Colors.white70),
            border: InputBorder.none,
          ),
        ),
        SizedBox(height: 20),
        SizedBox(height: 10,),
        TextButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.orangeAccent),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                )
            ),
          ),
          child: Text("Select Dates", style: TextStyle(color: Colors.white)),
          onPressed: () async {
            await displayDateRangePicker(context);
          },
        ),
        SizedBox(height: 5,),
        Text("Start Date: ${DateFormat('MM/dd/yyyy').format(_startDate).toString()}",
          style: TextStyle(
              fontSize: 17
          ),),
        SizedBox(height: 5,),
        Text("End Date: ${DateFormat('MM/dd/yyyy').format(_endDate).toString()}",
          style: TextStyle(
              fontSize: 17
          ),),
        SizedBox(height: 20),
        SizedBox(
          width: width,
          height: 50,
          child: TextButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Colors.orangeAccent),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  )
              ),
            ),
            child: Text("Add",style: TextStyle(
                color:Colors.white)),
            key: Key('CreateNewTask'),
            onPressed: () async {
              if (todoTitleController.text.isNotEmpty) {
                await DatabaseService()
                    .createNewTodo(todoTitleController.text.trim(), _startDate, _endDate);
                Navigator.pop(context);
              }
            },
          ),
        )
      ],
    );
  }
}

/*
Scaffold(backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        elevation: 0,
        title: Text("New Task", style: TextStyle(
            fontSize: 25
        ),),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context)=>TaskListPage()));},
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 30),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(7)),
              color: Colors.white
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 20,),
                Container(
                  padding: EdgeInsets.all(15),
                  color: Colors.grey.withOpacity(0.2),
                  child: TextField(
                    decoration: InputDecoration(
                        hintText: "Title",
                        border: InputBorder.none
                    ),
                    style: TextStyle(
                        fontSize: 18
                    ),
                  ),
                ),
                SizedBox(height: 15,),
                Container(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Description", style: TextStyle(
                          fontSize: 18
                      ),),
                      SizedBox(height: 10,),
                      Container(
                        height: 150,
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(topRight: Radius.circular(15), topLeft: Radius.circular(15), bottomLeft: Radius.circular(15), bottomRight: Radius.circular(15)),
                            border: Border.all(
                                color: Colors.grey.withOpacity(0.5)
                            )
                        ),
                        child: TextField(
                          maxLines: 6,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: " Add description here",
                          ),
                          style: TextStyle(
                              fontSize: 18
                          ),
                        ),
                      ),
                      SizedBox(height: 10,),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: Colors.orangeAccent),
                        child: Text("Select Dates"),
                        onPressed: () async {
                          await displayDateRangePicker(context);
                        },
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          SizedBox(height: 5,),
                          Text("Start Date: ${DateFormat('MM/dd/yyyy').format(_startDate).toString()}",
                            style: TextStyle(
                                fontSize: 17
                            ),),
                          SizedBox(height: 5,),
                          Text("End Date: ${DateFormat('MM/dd/yyyy').format(_endDate).toString()}",
                            style: TextStyle(
                                fontSize: 17
                            ),),
                        ],
                      ),
                      SizedBox(height: 15,),
                      SizedBox(
                        width: width,
                        height: 50,
                        child: TextButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(Colors.orangeAccent),
                            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                )
                            ),
                          ),
                          child: Text("Add",style: TextStyle(
                              color:Colors.white)),
                          onPressed: () async {
                            if (todoTitleController.text.isNotEmpty) {
                              await DatabaseService()
                                  .createNewTodo(todoTitleController.text.trim(), _startDate, _endDate);
                              Navigator.pop(context);
                            }
                          },
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),);}
 */
