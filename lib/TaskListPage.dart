import 'package:flutter/material.dart';
import 'package:i_am_rich/NewTask.dart';
import 'package:i_am_rich/Todo.dart';
import 'AllTaskPage.dart';
import 'database_services.dart';
import 'dart:developer' as developer;

class TaskListPage extends StatefulWidget {
  @override
  TaskListPageState createState() => TaskListPageState();
}

class TaskListPageState extends State<TaskListPage> {
  bool isComplet = false; // just for now
  TextEditingController todoTitleController = TextEditingController();
  String filterType = "Today";

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        resizeToAvoidBottomInset: false, // set it to false
        body: SingleChildScrollView(child:
          StreamBuilder<List<Todo>>(
          stream: DatabaseService().listTodos(),
          builder: (context, snapshot) {
            List<Todo> todos = snapshot.data;
            if (!snapshot.hasData) {
              return Center(
                  child: CircularProgressIndicator(),
              );}
            return Padding(
              padding: EdgeInsets.all(25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AppBar(
                    backgroundColor: Colors.grey[900],
                    elevation: 0,
                    title: Text("All Tasks", style: TextStyle(
                      fontSize: 30,
                      color: Colors.orangeAccent,
                      fontWeight: FontWeight.bold,
                    ),),
                    actions: [
                      IconButton(
                        icon: Icon(Icons.calendar_today,
                          color: Colors.orangeAccent,
                          size: 30,
                        ),
                        onPressed: null,
                      )
                    ],
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            onTap: (){changeFilter("Today");},
                            child: Text("Today", style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 18
                            ),),
                          ),
                          SizedBox(height: 10,),
                          Container(
                            height: 4,
                            width: 120,
                            color: (filterType== "Today")?Colors.orangeAccent:Colors.transparent,
                          )
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context)=> AllTaskPage()));},
                            child: Text("All Tasks", style: TextStyle(
                                color: Colors.orangeAccent,
                                fontSize: 18
                            ),),
                          ),
                          SizedBox(height: 10,),
                          Container(
                            height: 4,
                            width: 120,
                            color: (filterType == "All Tasks")?Colors.orangeAccent:Colors.transparent,
                          )
                        ],
                      )
                    ],
                  ),
                  Divider(
                    color: Colors.orange[200],
                  ),
                  ListView.separated(
                    physics: const AlwaysScrollableScrollPhysics(),
                    separatorBuilder: (context, index) => Divider(
                      color: Colors.orange[200],
                    ),
                    shrinkWrap: true,
                    itemCount: todos.length,
                    itemBuilder: (context, index) {
                      return Dismissible(
                        key: Key(todos[index].uid),
                        background: Container(
                          padding: EdgeInsets.only(left: 20),
                          alignment: Alignment.centerLeft,
                          child: Icon(Icons.delete),
                          color: Colors.red,
                        ),
                        onDismissed: (direction) async {
                          await DatabaseService()
                              .removeTodo(todos[index].uid);
                          //
                        },
                        child: ListTile(
                          onTap: () {
                            DatabaseService().completTask(todos[index].uid);
                          },
                          leading: Container(
                            padding: EdgeInsets.all(2),
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              color: Theme.of(context).primaryColor,
                              shape: BoxShape.circle,
                            ),
                            child: todos[index].isComplet
                                ? Icon(
                              Icons.check,
                              color: Colors.orangeAccent,
                            )
                                : Container(),
                          ),
                          title: Text(
                            todos[index].title,
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey[200],
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          subtitle: Text(
                            "text",//todos[index].startTime.toString() + "\n" + todos[index].endTime.toString(),
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey[200],
                              fontWeight: FontWeight.w600,
                            ),
                          )
                        ),
                      );
                    },
                  )
                ],
              ),
            );
          }),),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            key: Key('AddNewTaskButton'),
            backgroundColor: Theme.of(context).primaryColor,
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return NewTask();
            },
          );
        }
      ),
    );
  }

  changeFilter(String filter) {
    filterType = filter;
    setState(() {

    });
  }

}
/*
SimpleDialog(
              contentPadding: EdgeInsets.symmetric(
                horizontal: 25,
                vertical: 20,
              ),
              backgroundColor: Colors.grey[800],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              title: Row(
                children: [
                  Text(
                    "Add A Task",
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  ),
                  Spacer(),
                  IconButton(
                    icon: Icon(
                      Icons.cancel,
                      color: Colors.grey,
                      size: 30,
                    ),
                    onPressed: () => Navigator.pop(context),
                  )
                ],
              ),
              children: [
                Divider(),
                TextFormField(
                  controller: todoTitleController,
                  style: TextStyle(
                    fontSize: 18,
                    height: 1.5,
                    color: Colors.white,
                  ),
                  autofocus: true,
                  decoration: InputDecoration(
                    hintText: "eg. exercise",
                    hintStyle: TextStyle(color: Colors.white70),
                    border: InputBorder.none,
                  ),
                ),
                SizedBox(height: 20),
                SizedBox(
                  width: width,
                  height: 50,
                  child: TextButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.orangeAccent),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                      )
                      ),
                    ),
                    child: Text("Add",style: TextStyle(
              color:Colors.white)),
                    onPressed: () async {
                      if (todoTitleController.text.isNotEmpty) {
                        await DatabaseService()
                            .createNewTodo(todoTitleController.text.trim());
                        Navigator.pop(context);
                      }
                    },
                  ),
                )
              ],
            );}
 */

/**Scaffold(
      body: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppBar(
                backgroundColor: Colors.grey,
                elevation: 0,
                title: Text("All Tasks", style: TextStyle(
                    fontSize: 30,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                ),),
                actions: [
                  IconButton(
                    icon: Icon(Icons.calendar_today,
                      color: Colors.white,
                      size: 30,
                    ),
                  )
                ],
              ),
              Container(
                height: 70,
                color: Colors.orangeAccent,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        InkWell(
                          onTap: (){changeFilter("Today");},
                          child: Text("Today", style: TextStyle(
                              color: Colors.white,
                              fontSize: 18
                          ),),
                        ),
                        SizedBox(height: 10,),
                        Container(
                          height: 4,
                          width: 120,
                          color: (filterType== "Today")?Colors.white:Colors.transparent,
                        )
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        InkWell(
                          onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context)=>AllTaskPage()));},
                          child: Text("All Tasks", style: TextStyle(
                              color: Colors.white,
                              fontSize: 18
                          ),),
                        ),
                        SizedBox(height: 10,),
                        Container(
                          height: 4,
                          width: 120,
                          color: (filterType == "All Tasks")?Colors.white:Colors.transparent,
                        )
                      ],
                    )
                  ],
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      taskWidget(Colors.deepPurpleAccent, "meeting on Fri night" , "3:00 PM"),
                      taskWidget(Colors.deepPurpleAccent, "learn android development" , "7:00 PM"),
                      taskWidget(Colors.deepPurpleAccent, "learn flutter" , "everyday"),
                    ],
                  ),
                ),
              ),
              Container(
                height: 110,
                child: Stack(
                  children: [
                    Positioned(
                      bottom: 25,
                      left: 0,
                      right: 0,
                      child: InkWell(
                        key:  Key('AddNewTaskButton'),
                        onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context)=>NewTask()));},
                        child: Container(
                          height: 60,
                          width: 60,
                          decoration: BoxDecoration(
                              color: Colors.deepPurpleAccent,
                              shape: BoxShape.circle
                          ),
                          child: Center(
                            child: Text("+", style: TextStyle(
                                fontSize: 40,
                                color: Colors.white
                            ),),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
    changeFilter(String filter) {
      filterType = filter;
      setState(() {

      });
    }
  }

Slidable taskWidget(Color color, String title, String time)
{
  return Slidable(
    actionPane: SlidableDrawerActionPane(),
    actionExtentRatio: 0.3,
    child: Container(
      height: 80,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
              color: Colors.black.withOpacity(0.03),
              offset: Offset(0,9),
              blurRadius: 20,
              spreadRadius: 1
          )]
      ),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            height: 25,
            width: 25,
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                border: Border.all(color: color,
                    width: 4)
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(title, style: TextStyle(
                  color: Colors.black,
                  fontSize: 18
              ),),
              Text(time, style: TextStyle(
                  color: Colors.grey,
                  fontSize: 18
              ),)
            ],
          ),
          Expanded(child: Container(),),
          Container(
            height: 50,
            width: 5,
            color: color,
          )
        ],
      ),
    ),
    secondaryActions: [
    Container(
      height: 80,
      margin: EdgeInsets.symmetric(vertical: 10),
      child: IconSlideAction(
          caption: "Edit",
          color: Colors.white,
          icon: Icons.edit,
          onTap: (){},
        ),
    ),
    Container(
      height: 80,
      margin: EdgeInsets.symmetric(vertical: 10),
      child: IconSlideAction(
          caption: "Delete",
          color: color,
          icon: Icons.edit,
          onTap: (){},
        )
      )
    ],
  );
          **/

