import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:i_am_rich/NewTask.dart';
import 'package:i_am_rich/TaskListPage.dart';

void main() {
  group('Test Task List Page', () {
    testWidgets('Open create page when Plus Button is clicked', (WidgetTester tester) async {
      // WidgetsFlutterBinding.ensureInitialized();
      // await Firebase.initializeApp();
      // Build the widget
      await tester.pumpWidget(
          new MediaQuery(
              data: new MediaQueryData(),
              child: new MaterialApp(home: new TaskListPage())
          )
      );

      await tester.tap(find.byKey(Key('AddNewTaskButton')));

      await tester.pumpAndSettle();

      expect(find.byType(NewTask), findsOneWidget);

    });
  });
}
