import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:i_am_rich/CreateTaskPage.dart';
import 'package:i_am_rich/NewTask.dart';

void main() {
  testWidgets('create task page is created', (WidgetTester tester) async {
    final testWidget = MaterialApp(
      home: CreateTaskPage(),
    );
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
  });

  testWidgets('create task page contains Add A Task text', (WidgetTester tester) async {
    final testWidget = MaterialApp(
      home:CreateTaskPage(),
    );

    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();

    expect(find.text('Add A Task'), findsOneWidget);
  });

  testWidgets('create task page contains cancel icon button', (WidgetTester tester) async {
    final testWidget = MaterialApp(
      home:CreateTaskPage(),
    );

    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();

    expect(find.byType(IconButton), findsOneWidget);

    // buttonMaterial
    //
    // const iconButton = IconButton(icon: Icon(Icons.cancel), onPressed: null);
    //
    // expect(find.byWidget(IconButton), findsOneWidget);
  });

}