import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:i_am_rich/TaskListPage.dart';
import 'package:i_am_rich/main.dart';

void main() {
  testWidgets('task list page is created', (WidgetTester tester) async {
    final testWidget = MaterialApp(
      home: MyApp(),
    );
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
  });

  testWidgets('task list page contains all tasks text',
         (WidgetTester tester) async {
       final testWidget = MaterialApp(
         home: TaskListPage(),
       );

       await tester.pumpWidget(testWidget);
       await tester.pumpAndSettle();

       expect(find.text('All Tasks'), findsOneWidget);
  });

  testWidgets('task list page contains button', (WidgetTester tester) async {
    final testWidget = MaterialApp(
      home: TaskListPage(),
    );

    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();

    expect(find.byIcon(Icons.add), findsOneWidget);
    expect(find.byKey(Key('AddNewTaskButton')), findsOneWidget);
  });

  testWidgets('notify when button is pressed', (WidgetTester tester) async {
    var pressed = false;
    final testWidget = MaterialApp(
      home: FloatingActionButton(
        onPressed: () => pressed = true,
      ),
    );

    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();

    await tester.tap(find.byType(FloatingActionButton));
    await tester.pumpAndSettle();

    expect(pressed, isTrue);
  });

  testWidgets('notify when button is pressed', (WidgetTester tester) async {
    var pressed = false;
    final testWidget = MaterialApp(
      home: IconButton(
        onPressed: () => pressed = true,
      ),
    );

    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();

    await tester.tap(find.byType(IconButton));
    await tester.pumpAndSettle();

    expect(pressed, isTrue);
  });

}